function result = io_isfull(buffer)
    result = isempty(find(isnan(buffer), 1));
end