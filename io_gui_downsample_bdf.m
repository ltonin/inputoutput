function [outbdf] = io_gui_downsample_bdf(dstfreq, srcpath, dstpath)
% [outbdf] = io_gui_downsample_bdf(dstfreq, srcpath, dstpath)
%
% A gui wrapper function to bandpass bdf files.
%
% dstfreq      -- destination bdf sampling rate
% srcpath      -- initial path where to open the selection window [optional]
% dstpath      -- initial path where to open the save window [optional]
%
% N.B. Source and destination path must be different if you DO NOT want to
% override the file
%
% SEE ALSO: io_downsample_bdf

    if nargin == 1
        srcpath = './';
        dstpath = './';
    end
    
    if nargin == 2
        dstpath = './';
    end

    [files, srcpath] = uigetfile([srcpath '*.bdf'], '[io] - Select bdf(s) to bandpass', 'MultiSelect', 'on');
    
    [dstpath] = uigetdir([dstpath '/'], '[io] - Select directory to save bandpass bdf(s) NOT the same!');
    
    if iscell(files)
        NumFiles = length(files);
        filelist = files;
    else
        NumFiles = 1;
        filelist{1} = files;
    end
    
    disp([num2str(NumFiles) ' bdf selected. Downsampling to ' num2str(dstfreq) ' Hz']);
    
    outbdf = cell(1, NumFiles);
    
    for fId = 1:NumFiles

        c_srcbdf = [srcpath filelist{fId}];
        c_dstbdf = [dstpath '/' filelist{fId}];
        
        disp(['[io] - Downsampling ' num2str(fId) '/' num2str(NumFiles) ' bdf: ' c_srcbdf])
        io_downsample_bdf(c_srcbdf, c_dstbdf, dstfreq);
        disp(['[io] - Output bdf: ' c_dstbdf])

        outbdf{fId} = c_dstbdf;
    end
    
    
end