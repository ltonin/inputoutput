function [dsthd] = io_downsample_bdf(srcfile, dstfile, dstfreq)
% [dsthd] = io_downsample_bdf(srcfile, dstfile, dstfreq)
%
% This function downsample to a given sample rate a given bdf file. Then 
% the new bdf file is saved in a given location. To downsample the signal a
% decimate function is used. To downsample trigger channel downsample
% function is used.
%
% srcfile      -- bdf source file
% dstfile      -- bdf destination file
% dstfreq      -- destination bdf sampling rate
%
% The function returns the header of the destination file.
%
% SEE ALSO: readbdfheader, allocbdffile, writebdfheader, readbdfdata,
% getbdfchannels

    srchd = readbdfheader(srcfile); 

    srcfreq = srchd.SamplingRate;
    DownRatio = round(srcfreq/dstfreq);
    
    dsthd = srchd;
    dsthd.filename = dstfile;
    
    dsthd.NumSamplesPerRecord = srchd.NumSamplesPerRecord/DownRatio;
    dsthd.SamplingRate = dsthd.NumSamplesPerRecord / dsthd.dataDuration;

    for ChId = 1:dsthd.numberChannels
        dsthd.Channel(ChId).NumSamplesPerRecord = dsthd.NumSamplesPerRecord;
        dsthd.Channel(ChId).SamplingRate = dsthd.SamplingRate;
    end
    
    allocbdffile(dsthd);
    writebdfheader(dsthd);
        
    
    Channels = getbdfchannels(srchd,'analog');
    
    for ChId = Channels'
        
        s = readbdfdata(srchd, 0, ChId);
        writebdfdata(dsthd, decimate(s, DownRatio), 0, ChId);
        
    end
    
    
    TrigChannel = getbdfchannels(srchd, {'Status'});
    trig = readbdfdata(srchd, 0, TrigChannel);
    writebdfdata(dsthd, downsample(trig, DownRatio), 0, TrigChannel);
    
    
end