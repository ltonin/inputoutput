function [outbdf] = io_gui_import_bdf(channels, trigger, srcpath)
% [outbdf] = io_gui_bandpass_bdf(order, band, [srcpath, dstpath])
%
% A gui wrapper function to bandpass bdf files.
%
% order        -- filter order
% band         -- vector with start and stop frequency [Hz]
% srcpath      -- initial path where to open the selection window [optional]
% dstpath      -- initial path where to open the save window [optional]
%
% N.B. Source and destination path must be different if you DO NOT want to
% override the file
%
% SEE ALSO: io_bandpass_bdf

    if nargin == 2
        srcpath = './';
    end
    

    [files, srcpath] = uigetfile([srcpath '*.bdf'], '[io] - Select bdf(s) to import', 'MultiSelect', 'on');
    
    if iscell(files)
        NumFiles = length(files);
        filelist = files;
    else
        NumFiles = 1;
        filelist{1} = files;
    end
    
    disp([num2str(NumFiles) ' bdf selected. Butterworth bandpass filter - order: ' num2str(order) ' - band: ' num2str(band(1)) '-' num2str(band(2)) ' Hz']);

    outbdf = cell(1, NumFiles);
    
    for fId = 1:NumFiles

        c_srcbdf = [srcpath filelist{fId}];
        c_dstbdf = [dstpath '/' filelist{fId}];
        
        disp(['[io] - Bandpassing ' num2str(fId) '/' num2str(NumFiles) ' bdf: ' c_srcbdf])
        io_bandpass_bdf(c_srcbdf, c_dstbdf, order, band);
        disp(['[io] - Output bdf: ' c_dstbdf])
        
        outbdf{fId} = c_dstbdf;
    end
    
    
    
end