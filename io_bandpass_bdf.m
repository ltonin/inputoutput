function [dsthd] = io_bandpass_bdf(srcfile, dstfile, order, band)
% [dsthd] = io_bandpass_bdf(filepath, dstpath, order, band)
%
% This function bandpass in a given frequency band a bdf file. Then the new
% bdf file is saved in a given location. To bandpass the signal a
% butterworth filter is used.
%
% srcfile      -- bdf source file
% dstfile      -- bdf destination file
% order        -- filter order
% band         -- vector with start and stop frequency [Hz]
%
% The function returns the header of the destination file.
%
% SEE ALSO: readbdfheader, allocbdffile, writebdfheader, readbdfdata,
% getbdfchannels, filt_bp

    srchd = readbdfheader(srcfile); 

    dsthd = srchd;
    dsthd.filename = dstfile;
    
    
    allocbdffile(dsthd);
    writebdfheader(dsthd);
    
    
    fs = dsthd.SamplingRate;
    
    Channels = getbdfchannels(srchd, 'analog');
    
    for ChId = Channels'     
        
        s = readbdfdata(srchd, 0, ChId);
        s = s';
      
        bp_s = filt_bp(s, order, band, fs);
        writebdfdata(dsthd, bp_s', 0, ChId);      
        
    end
    
    TrigChannel = getbdfchannels(srchd, {'Status'});
    trig = readbdfdata(srchd, 0, TrigChannel);
    writebdfdata(dsthd, trig, 0, TrigChannel);
    


end