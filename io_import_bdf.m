function [s, h] = io_import_bdf(filepath, channels)
% [s, h] = io_import_bdf(filepath, [channels])
%
% Import data and events from bdf file. 
%
% filepath      path to the bdf file
% channels      ids of channel to be imported  OR cell array with channels name [default all channels]
%
% The function returns:
%
% s             Imported signal [samples x channels]
% h             Header structure with the following fields:
%                   .format     file format ('bdf')
%                   .filename   Full filename
%                   .EVENT      Event structure
%                   .SampleRate Sampling rate
%
% SEE ALSO: readbdfheader, readbdfdata, getbdfchannels, io_event_bdf,
% io_read_bdf

    if nargin == 1
        channels = [];
    end

    [s, header] = io_read_bdf(filepath, channels);
    [itrig]     = io_read_bdf(filepath, {'Status'});
  
    events = io_event_bdf(itrig);
    

    h.format = 'bdf';
    h.file   = filepath;
    h.EVENT = events;
    h.SampleRate = header.SamplingRate;
    

end