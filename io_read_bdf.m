function [s, header] = io_read_bdf(filepath, channels)
% [s, header] = io_read_bdf(filepath, channels)
%
% Read channel(s) from bdf file. 
%
% filepath      -- path to the bdf file
% channels      -- ids of channel to be imported  OR cell array with channels name [default all channels]
%
% The function returns:
%
% s             -- pointers x channels array
% header        -- Header of the file (BDF format)
%
% SEE ALSO: readbdfheader, readbdfdata, getbdfchannels

    header = readbdfheader(filepath);
    
    if nargin == 1 || isempty(channels)
        channels = 1:header.numberElectrods;
    end
    
    
    if iscell(channels) 
        channels = getbdfchannels(header, channels);
    end
    
    s = readbdfdata(header, 0, channels);
    s = s';
    
end