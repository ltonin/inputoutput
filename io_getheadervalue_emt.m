function val = io_getheadervalue_emt(header, pattern, delimiter, format)
% val = io_getheadervalue_emt(header, pattern, [delimiter, format])
% 
% Given a EMT text header, it returns the value associated to a given
% patter. 
% 
% Ex:
% >> h.TYPE = io_getheadervalue_emt(htext, 'Type')
% >> ans = 
% >> Emg tracks
%
% By default, delimiter [Optional] is set to ':' and the format [Optional] 
% of the header's rows is '%s%s'.
%
% SEE ALSO: io_import_emt

    if nargin < 3
        delimiter = ':';
        format = '%s%s';
    end
    
    if nargin < 4
        format = '%s%s';
    end

    hid   = util_cellfind(header, {pattern});
    hcell = textscan(header{hid, 1}, format, 'Delimiter', delimiter);
    val    = char(hcell{2});
end