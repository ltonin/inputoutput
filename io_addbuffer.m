function buffer = io_addbuffer(buffer, frame)  
    [Sb, Cb] = size(buffer);
    [Sf, Cf] = size(frame);

    if(Cf == 0 || Cb == 0)
        return;
    end

    if(Cb ~= Cf)
        disp('[io_addbuffer] Error: channel size does not match');
        return;
    end

    if(Sf > Sb)
        disp('[io_addbuffer] Error: frame larger than buffer');
        return;
    end

    buffer = [buffer(Sf+1:end, :); frame];

end