function [s, h] = io_import_emt(filename, Delimiter, HeaderLength)
% [s, h] = io_import_emt(filename, [Delimiter, HeaderLenght])
%
% Import data and header from emt file.
%
% filename      -- Name of emt file
% Delimiter     -- [Optional] delimiter character between column. By
%                  default is tab ('\t')
% HeaderLength  -- [Optional] Length of the header text part (rows). By 
%                  default is 11.
%
% The function returns:
%
% s             -- samples x channels array
% h.FILENAME    -- filename
% h.TYPE        -- type of data
% h.DATALABELS  -- labels of channels
% h.UNITS       -- measure units of data
% h.SAMPLERATE  -- recording sampling rate
% h.FRAMES      -- total number of recorded frames
% h.SAMPLES     -- vector of the same length of s with the recorded frames
%                  (starting from the beginning of the file)
% h.TIME        -- vector of the same length of s with the associated
%                  timestamps (starting from the beginning of the file)
%
% SEE ALSO: io_getheadervalue_emt, io_gettrigger_emt

    if nargin < 2
        Delimiter = '\t';
        HeaderLength = 11;
    end
    
    if nargin < 3
        HeaderLength = 11;
    end
    
    fdata = importdata(filename, Delimiter, HeaderLength);

    % Data structure
    samples = fdata.data(:, 1);
    time    = fdata.data(:, 2);
    s       = fdata.data(:, 3:end);
    
    htext   = fdata.textdata(:, 1);
    hcol    = fdata.colheaders(3:end);
    
    
    % Header structure
    
    h.FILENAME   = filename;
    h.TYPE       = io_getheadervalue_emt(htext, 'Type');
    h.DATALABELS = strtrim(hcol);
    h.UNITS      = io_getheadervalue_emt(htext, 'Measure unit');
    h.SAMPLERATE = io_getheadervalue_emt(htext, 'Frequency');
    h.FRAMES     = str2double(io_getheadervalue_emt(htext, 'Frames')); 
    h.SAMPLES    = samples + 1;
    h.TIME       = time;
    
    
end

