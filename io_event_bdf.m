function event = io_event_bdf(s)
% event = io_event_bdf(s)
%
% The function extracts events from digital bdf status signal. It retrieves
% information about the type of the event, the position and the duration.
%
% The function returns an event structure as follows:
%
% event.TYP     Type of the event
% event.POS     Position of the raising edge of the event
% event.DUR     Duration of the event
%
% SEE ALSO: io_import_bdf, io_read_bdf

    
    dtrig = diff(bitand(hex2dec('FF'), s));
    
    utyp  = unique(dtrig(dtrig>0));
    nutyp = length(utyp);
    
    typ = [];
    pos = [];
    dur = [];
    for tId = 1:nutyp
       cstart = find(dtrig == utyp(tId));
       cstop  = find(dtrig == -utyp(tId));
       pos    = cat(1, pos, cstart);
       dur    = cat(1, dur, cstop - cstart);
       typ    = cat(1, typ, utyp(tId).*ones(length(cstart), 1));
    end
    
    [pos, index] = sort(pos);
    typ = typ(index);
    dur = dur(index);
    
    event.TYP = typ;
    event.POS = pos;
    event.DUR = dur; 
end